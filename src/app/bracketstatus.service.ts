import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class BracketstatusService {
  private messageSource = new BehaviorSubject('deactive');
  currentMessage = this.messageSource.asObservable();

  private htmlSource=new BehaviorSubject('<p>');
  currentHtml=this.htmlSource.asObservable();
  constructor() { }
  changeMessage(message: string) {
    this.messageSource.next(message)
  }
    
  changehtml(message:string)
  {
    this.htmlSource.next(message)
  }
}
