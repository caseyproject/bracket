import { Component, OnInit,EventEmitter, Input, Output } from '@angular/core';
import{SecurityServiceService}from'../../security-service.service' 
import * as moment from 'moment'; 
import {LoadingBarService, LoadingBarModule} from "ngx-loading-bar";
import {MatDialog} from '@angular/material/dialog';
import{DeletepopupComponent}from'../../deletepopup/deletepopup.component'
import { MatSlideToggleChange } from '@angular/material';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-show-tournament',
  templateUrl: './show-tournament.component.html',
  styleUrls: ['./show-tournament.component.css']
})
export class ShowTournamentComponent implements OnInit {
  tournament=[];

collection=[];
@Input() id: string;
@Input() maxSize: number;
@Output() pageChange: EventEmitter<number>;
height = 4;
color = "#4092F1";
runInterval = 3000;
name: string;
tournamentPmt=[]
tournament_eventtype
  constructor(public router:Router,public dialog: MatDialog,public security:SecurityServiceService,private loadingBarService: LoadingBarService,) {
    
    for (let i = 1; i <= 100; i++) {
      this.collection.push(`item ${i}`);
    }

    this.tournament=[];
  


   }

  ServerTimestamp(nowDate) {
    return moment(nowDate).format("MMM DD,YYYY");    //Sep 29,2019
  } 
  srcText(val){  
    console.log("val=",val);
    if (val && val.trim() != '') {
      this.tournament = this.tournamentPmt.filter((item) => {   
        return (item.event_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.tournament = this.tournamentPmt;
    }
  }
  ngOnInit() {
    this.loadonpage()
  }
  loadonpage()
  {
    this.security.GetAllTournament(0,10).subscribe(data =>{
      if(data["status"]=="200"){ 
        console.log("data==",data);  
        this.tournament=data["tournament"];
        this.tournamentPmt=data["tournament"];
        console.log("data tournament==",data["tournament"]);  
      }
  })
  }
  delete(i)
  {
    var id=this.tournament[i].id
    const dialogRef = this.dialog.open(DeletepopupComponent, { 
      disableClose : false,
      data: {name: 'on'}
    });
    dialogRef.beforeClose().subscribe(result => {  
      console.log('The dialog beforeClose');
      });
    dialogRef.afterClosed().subscribe(result => {
      console.log("Dialog result:", result);
      if(result=='on'){
      this.security.delete(id).subscribe(data =>{
        if(data["status"]=="200"){ 
          this.loadonpage()
        }
    })
  }
     
    });
  }


  toggle(event: MatSlideToggleChange,id)
  { console.log('toggle', event.checked+'+'+id)
 
  // this.tournament[id].tourPublishState=event.checked
  var tourPublishState
  if(event.checked==false)
  {
   tourPublishState=2
   
  }
  else
  {
    tourPublishState=1 
  }
  this.tournament[id].tourPublishState=tourPublishState
  this.security.publishapi(this.tournament[id].id,tourPublishState).subscribe(data=>{
    
    // this.loadonpage()
  })
  console.log('security',this.security)
  }
  edit(id)
  {
    this.security.editapi(this.tournament[id].id).subscribe(data=>{
      console.log('data',data)
      this.navigatetournament(data)
      // this.router.navigate(['create-tournament']);
    })
  
  }
  navigatetournament(data)
  {
    console.log('tournament',data.tournament.event_name)
    var DetailonSubmitArr=[]; 
   
    DetailonSubmitArr.push({EventNameCtrl:data.tournament.event_name,
    OptionCtrl:data.tournament.event_type,
    SportCtrl:data.tournament.sport_id,
    timeDetail:data.tournament.time_duration,
    startDate:data.tournament.startDate,
    endDate:data.tournament.endDate
  });
  localStorage.setItem('id',data.tournament.id)
  
    localStorage.setItem('DetailonSubmitArr',JSON.stringify(DetailonSubmitArr)); 
    this.router.navigate(['create-tournament']);
  }
}
