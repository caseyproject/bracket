import { Component, OnInit, Input } from '@angular/core';
 

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SecurityServiceService } from 'src/app/security-service.service';

 
@Component({
  selector: 'app-buy-credits',
  templateUrl: './buy-credits.component.html',
  styleUrls: ['./buy-credits.component.css']
})

export class BuyCreditsComponent implements OnInit {

  parentMessage ;
  user;

  constructor(private Products:SecurityServiceService,
    private route: ActivatedRoute,
    private location: Location
    ) { }
    
  package =[];
  getItems(): void {
    this.Products.getPrice().subscribe((data )=> {
      this.package = data['package'];
    });
    
  }

  item : any;

  addToCart(id:number) {
    // console.log("addToCart click item is", this.item);
    //this.item =id;

  //this.Products.addToCart(this.item)
  //console.log(id);
 
  }

  ngOnInit() {
    this.getItems();
    
  this.user = JSON.parse(localStorage.getItem('data'));
     
  console.log("This is amount and Token id",this.user.amount);
  }

private loaded = false;
loadMyChildComponent(id){
this.loaded=true;

this.parentMessage =id ;
//alert(this.parentMessage)
}

goBack(): void {
this.location.back();
}


}
