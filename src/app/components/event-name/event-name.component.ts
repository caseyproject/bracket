import { Component, OnInit ,ViewChild,ElementRef} from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid'; 
import interactionPlugin ,{Draggable} from '@fullcalendar/interaction'
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import{DialogpopupComponent}from'../../dialogpopup/dialogpopup.component'
import { from } from 'rxjs';
import { Router } from '@angular/router';
import{Observable,Subject}from'rxjs'
import{EventnameService}from'../../eventname.service';
import {LoadingBarService, LoadingBarModule} from "ngx-loading-bar";
import {MatSnackBar} from '@angular/material/snack-bar';
import{SecurityServiceService}from'../../security-service.service';
//import { NgxSpinnerService } from "ngx-spinner"; 

export interface DialogData {
  divisionname: string;
}
@Component({
  selector: 'app-event-name',
  templateUrl: './event-name.component.html',
  styleUrls: ['./event-name.component.css']
})
export class EventNameComponent implements OnInit {

  disabled;
  color;
  checked;

  divisionname: string;
  eventname:string;
  options: any;
  eventsModel: any;
  divisionitem=[]
  @ViewChild('fullcalendar') fullcalendar: FullCalendarComponent;
  @ViewChild('external') external: ElementRef;
  items = [{venue:'Venue',date:'30-09-2019',starttime:'10:00',endtime:'13:00'},{venue:'Venue',date:'30-10-2019',starttime:'10:00',endtime:'13:00'}];
  finalarrayvenue;
  addteams;
  editinput:boolean;
  buttonchange:boolean;
  editarrayinput=[{idteams:'Team 0'}];
  idteams=0;
  eventarray=[];
  eventid;
  evennamearray=[];
  teams=[];
  publishdetails=[];

  eventtitle:any=null;   eventtitleID:any=null;

  eventnameinput;
  eventinputarray=[];
  eventinputsave;
  geteventid;
  eventgetid=0;
  buttonactive:boolean=false;
  headeractive:boolean;

  DivisionBodyBool:boolean=false;
  eventlength;
  isActive:boolean;
  publishArray=[];

tournamentID:any=null;   

/* chetan's Code */
sum:number =0;  
divArr=[];  
enableDivPublish = true;   
DivPublishArr:any=[];

divPublishArr=[];
enableDivUnPublish= true;
sumUnpublish:number=0;
/* end of chetan's code */
availblecredit
  constructor(private loadingBarService: LoadingBarService,public eventnames:EventnameService,public dialog: MatDialog,public router:Router,public security:SecurityServiceService,private _snackBar: MatSnackBar) {
    this.eventlength=this.evennamearray.length
    console.log('eventlength',this.eventlength)
    this.editinput=false;
    this.buttonchange=true;
    this.eventinputsave=false;  

    // this.eventtitle="General";  
    this.DivisionBodyBool=true;

    this.buttonactive=false; 
    this.headeractive=true;
    this.isActive = false;
    this.tournamentID = localStorage.getItem("tournament_id"); 
    this.security.CheckInternetCoonection().subscribe(isOnline => {
      if(isOnline){  
       this.ConstructorFun();
       }
      else {   this._snackBar.open('Internal Server Error', 'Error', {  duration: 10000  });    }
    }); 

    //if (localStorage.getItem('evennamearray') != null) {   localStorage.removeItem('evennamearray');  }  
    /*
    var eventArrayTmp=JSON.parse(localStorage.getItem('evennamearray'));
    console.log("eventArrayTmp=",eventArrayTmp);
    if(eventArrayTmp!=null) {         
      this.evennamearray=JSON.parse(localStorage.getItem('evennamearray'));
    }
    else {
      this.eventarray.push({ divisionitem:this.divisionitem[0],inputteams:null });
      // this.evennamearray.push({ divisionitem:'General' });
        console.log('eventarray==',this.eventarray);
    }
     */
    console.log('this.evennamearray after',this.evennamearray); 
  }

  ConstructorFun() {
    this.security.GetAllListing(this.tournamentID).subscribe(data =>{  
      if(data["status"]=="200"){   
        console.log("data==",data["division"]);

        var DivisionItem=[];
        var eventinputarray=[];
      for(let i=0;i<data["division"].length;i++) { 
          eventinputarray=this.DivisionTeams(data["division"][i].id,data['team']);
          DivisionItem.push({divisionitem:data["division"][i].division_name,id:data["division"][i].id,eventinputarray:eventinputarray});
          
      }
      this.evennamearray=DivisionItem; 
        console.log("DivisionItem chetan==",DivisionItem);   
       
      }
    }, err => {    }) 
 
      
      
      this.security.GetPublish(this.tournamentID).subscribe((data )=> {
      
        this.teams = data['division_team'];
        console.log("this is division_team  info bro " , this.teams);
        
      });  

  }

  GetDivision(){
    this.security.GetPublish(this.tournamentID).subscribe((data )=> {
      this.teams = data['division_team'];
    });
    this.security.GetDivisionPublish().subscribe((data )=> {
      this.DivPublishArr = data['division'];
    });
  }

  DivisionTeams(DivisionID,TeamArr) { 
    var srcanswers=TeamArr.filter((mitem) => {
      return (mitem.division_id.toLowerCase().indexOf(DivisionID.toLowerCase()) > -1);
     })
     return srcanswers;
  }
   
  editteams() {
    this.buttonchange=false
    this.editinput=true
    this.buttonactive=true
    this.eventinputsave=true
    console.log(this.eventinputarray.length)
    if(this.eventinputarray.length==0)
    {
    this.idteams++
    this.eventinputarray.push({inputid:this.idteams,id:null})
    console.log(this.eventinputarray)
    for(var i=0;i<this.evennamearray.length;i++)
    {
     if(this.evennamearray[i].divisionitem==this.eventtitle)
     {
       this.geteventid=i
       this.evennamearray[i].eventinputarray=this.eventinputarray
     }
     }
    }
    console.log(this.evennamearray) 
  }

  addteamsfunc() {  
    this.eventinputsave=true
    this.idteams++
    this.eventinputarray.push({ inputid:this.idteams,id:null })
    console.log(this.eventinputarray)
    for(var i=0;i<this.evennamearray.length;i++) {
     if(this.evennamearray[i].divisionitem==this.eventtitle)  {
       this.geteventid=i
       this.evennamearray[i].eventinputarray=this.eventinputarray
     }
     }
    console.log(this.evennamearray)
  }

  delete(j) {       
    if(navigator.onLine) {  
            if(this.evennamearray[this.eventgetid].eventinputarray[j].id !=null) {
              this.security.TeamDelete(this.evennamearray[this.eventgetid].eventinputarray[j].id).subscribe(data =>{
                if(data["status"]=="200") { 
                  this._snackBar.open("Team deleted successfully.",'', {  duration: 5000  }); 
                  this.evennamearray[this.eventgetid].eventinputarray.splice(j,1);
                  if(this.evennamearray[this.eventgetid].eventinputarray==0) {
                    this.headeractive=false;
                   }
                  localStorage.setItem('evennamearray', JSON.stringify(this.evennamearray)); 
      
                } else {
                  this._snackBar.open('Internal Server Error', 'Error', {  duration: 10000  });   
                } 
              })
            }
    }
    else {   this._snackBar.open('No internet connection.', 'You are Offline', {  duration: 10000  });  }  
  }

  save()  {  
   this.eventinputsave=false;
   this.buttonactive=false;
   this.buttonchange=true; 
   this.eventinputarray=this.evennamearray[this.geteventid].eventinputarray;    
    if(this.evennamearray[this.geteventid].eventinputarray==0)  {
        this.editinput=false;
        this.addteams=false;
    }
    else  { 
      this.addteams=true; 
    } 
    console.log("chetan==",this.evennamearray[this.eventgetid].eventinputarray);
    if(navigator.onLine) {
  // add team code here ...
  // this.loadingBarService.start();     
    for(let i=0;i<this.evennamearray[this.eventgetid].eventinputarray.length;i++) {
        if(this.evennamearray[this.geteventid].eventinputarray[i].name=="" || this.evennamearray[this.geteventid].eventinputarray[i].name==null) { 
          return; 
        }
          else {      
          if(this.evennamearray[this.geteventid].eventinputarray[i].id==null) {
            this.security.TeamSave(this.evennamearray[this.geteventid].eventinputarray[i].name,this.tournamentID,this.eventtitleID).subscribe(data =>{
              if(data["status"]=="200") { 
                this.evennamearray[this.eventgetid].eventinputarray[i].id=data['team_list'].team_id;
                localStorage.setItem('evennamearray', JSON.stringify(this.evennamearray));     
              }
            })
          }
      else {     
        this.security.TeamUpdate(this.evennamearray[this.geteventid].eventinputarray[i].id,this.evennamearray[this.geteventid].eventinputarray[i].name).subscribe(data =>{
          if(data["status"]=="200") { 
            this.evennamearray[this.eventgetid].eventinputarray[i].id=data['team_list'].team_id;
            localStorage.setItem('evennamearray', JSON.stringify(this.evennamearray));     
          }  
        })
      }
    }
      if(i==(this.evennamearray[this.eventgetid].eventinputarray.length-1)){  
        this._snackBar.open('Teams saved successfully','', {  duration: 5000  }); 
        // this.loadingBarService.complete();  
      }
    } 
  }
  else {   this._snackBar.open('No internet connection.', 'You are Offline', {  duration: 10000  });  } 
  }



  DivisionBtn() {   
    console.log("this.eventtitle=",this.eventtitle);
    console.log("this.eventtitleID=",this.eventtitleID); 
    console.log("this.eventgetid==",this.eventgetid);
    const dialogRef=this.dialog.open(DialogpopupComponent, {  
      width:'350px', 
      height:'310px',  
      data: { divisionname:this.eventtitle,landingpage:"update" } 
   })
   dialogRef.afterClosed().subscribe(result => {  
    console.log('The dialog was closed==',result); 
    if(result != undefined) { 
    if(navigator.onLine) { 
      var tie_breakers=[];
      // this.loadingBarService.start()
      this.security.DivisionUpdate(result,this.eventtitleID).subscribe(data =>{
        if(data["status"]=="200") {    
          // this.loadingBarService.complete()
          this.evennamearray[this.eventgetid].divisionitem=result;
          this.eventlength=this.evennamearray.length
          console.log('eventlength',this.eventlength)
          this.eventtitle=result;
            console.log("result==popup==",result);
            localStorage.setItem('evennamearray', JSON.stringify(this.evennamearray)); 
            console.log("evennamearray==",JSON.parse(localStorage.getItem('evennamearray')));  
              console.log('this.evennamearray ==',this.evennamearray);  
                this._snackBar.open(data['mesg'],'', {  duration: 5000  });  
          return;    
        } else {
          this._snackBar.open('Internal Server Error', 'Error', {  duration: 10000  });   
        } 
      })
    }
    else {   this._snackBar.open('No internet connection.', 'You are Offline', {  duration: 10000  });  }  
    }
  });
  }

  navigatetobracket() {
    localStorage["divisonId"]=this.eventtitleID
    // this.eventnames.changemessage(this.eventtitleID)
    // this.eventnames.changemessage(JSON.stringify(this.evennamearray[0].eventinputarray))
    // this.eventnames.addtocart('product')
        //  this.subject.next({eventname:this.evennamearray})
    this.router.navigate(['/create-new-bracket'])
  }
 
  chooseevent(eventname,i) { 
    this.eventgetid=i; 
    console.log('eventname='+eventname);   
   this.eventtitle=eventname;
   this.eventinputsave=false;
   this.eventinputarray=[]; 
   this.eventtitleID=this.evennamearray[i].id
   console.log('this.evennamearray[i].id=',this.evennamearray[i].id);
   console.log('ismein kya hai=',this.evennamearray[i].eventinputarray);
   this.DivisionBodyBool=true; 
   this.eventlength=this.evennamearray[i].length;
   console.log('eventlength click=',this.eventlength);  
   document.getElementById('x_'+i).style.backgroundColor='#f8f9fa !important;';
 
   if(this.evennamearray[i].eventinputarray.length !=0) {   
   this.eventinputarray=this.evennamearray[i].eventinputarray;
   this.editinput=true;
   this.addteams=true; 
   }
   else { 
    this.editinput=false;
    this.addteams=false;
   }
  }

  newdivision()  { 
    console.log('division')
   const dialogRef=this.dialog.open(DialogpopupComponent, {  
      width:'350px', 
      height:'310px',  
      data:{ divisionname:this.divisionname,landingpage:"add" } 
   })
   dialogRef.afterClosed().subscribe(result => {  
    console.log('The dialog was closed==',result); 
    if(result != undefined) {
    if(navigator.onLine) { 
      var tie_breakers=[];
      // this.loadingBarService.start()
      this.security.DivisionSave(result,localStorage.getItem("tournament_id"),tie_breakers).subscribe(data =>{
        if(data["status"]=="200") { 
          this.eventtitle=result
          this.eventtitleID=data['division_list'].division_id
          // this.loadingBarService.complete()
          this._snackBar.open(data['mesg'],'', {  duration: 5000  });  
          var tmparr=[]; 
          this.evennamearray.push({divisionitem:result,id:data['division_list'].division_id,eventinputarray:tmparr});
          this.eventlength=this.evennamearray.length
          localStorage.setItem('evennamearray', JSON.stringify(this.evennamearray)); 
          console.log("evennamearray==",JSON.parse(localStorage.getItem('evennamearray')));  
            console.log('this.evennamearray ==',this.evennamearray);     
          return;    
        } else {
          this._snackBar.open('Internal Server Error', 'Error', {  duration: 10000  });   
        } 
      })
    }
    else {   this._snackBar.open('No internet connection.', 'You are Offline', {  duration: 10000  });  }  
    }
  });
  }
  user;

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('data'));
     
    console.log("This is amount and Token id",this.user.amount);
     this.availblecredit=this.user.amount

    this.eventname='General'
    this.addteams=false
    this.finalarrayvenue=JSON.parse(localStorage.getItem('FinalArrayVenue'))
    console.log('finalarrayvenue',JSON.parse(localStorage.getItem('FinalArrayVenue')))
    // Don't use FullcalendarOption interface
    this.options = {
      editable: true,
      customButtons: {
        myCustomButton: {
          text: 'custom!',
          click: function () {
            alert('clicked the custom button!');
          }
        }
      },
      theme: 'standard', // default view, may be bootstrap
      header: {
        left: 'prev,next today myCustomButton',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
        // right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
      eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

        alert(
            event.title + " was moved " +
            dayDelta + " days and " +
            minuteDelta + " minutes."
        );

        if (allDay) {
            alert("Event is now all-day");
        }else{
            alert("Event has a time-of-day");
        }

        if (!confirm("Are you sure about this change?")) {
            revertFunc();
        }

    },
      // defaultDate: '2019-10-12',
    // businessHours: true, // display business hours
    
    // events: [
    //   {
    //     title: 'Business Lunch',
    //     start: '2019-10-03T13:00:00',
    //     constraint: 'businessHours'
    //   },
    //   {
    //     title: 'Meeting',
    //     start: '2019-10-13T11:00:00',
    //     constraint: 'availableForMeeting', // defined below
    //     color: '#257e4a'
    //   },
    //   {
    //     title: 'Conference',
    //     start: '2019-10-18',
    //     end: '2019-10-20'
    //   },
    //   {
    //     title: 'Party',
    //     start: '2019-10-29T20:00:00'
    //   },

    //   // areas where "Meeting" must be dropped
    //   {
    //     groupId: 'availableForMeeting',
    //     start: '2019-10-11T10:00:00',
    //     end: '2019-10-11T16:00:00',
    //     rendering: 'background'
    //   },
    //   {
    //     groupId: 'availableForMeeting',
    //     start: '2019-10-13T10:00:00',
    //     end: '2019-10-13T16:00:00',
    //     rendering: 'background'
    //   },

    //   // red areas where no events can be dropped
    //   {
    //     start: '2019-10-24',
    //     end: '2019-10-28',
    //     overlap: false,
    //     rendering: 'background',
    //     color: '#ff9f89'
    //   },
    //   {
    //     start: '2019-10-06',
    //     end: '2019-10-08',
    //     overlap: false,
    //     rendering: 'background',
    //     color: '#ff9f89'
    //   }],
      // columnHeaderHtml: () => {
      //     return '<b>Friday!</b>';
      // },
      // add other plugins
      plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin, resourceTimeGridPlugin]
    };
    
    new Draggable(this.external.nativeElement, {
      itemSelector: '.fc-event',
      eventData: function(eventEl) {
        console.log(eventEl)
        return {
          title: eventEl.innerText
        };
      }
  });
  }
  
  selectitem(itemname,i)
  {
    console.log('get id',i)
    this.eventid=i
    this.eventname=itemname
   console.log('name',itemname)
  }
  eventClick(model) {
    console.log(model);
  }
  eventDragStop(model) {
    console.log('sdsds'+model);
  }
  dateClick(model) {
    console.log(model);
  }
  updateHeader() {
    this.options.header = {
      left: 'prev,next myCustomButton',
      center: 'title',
      right: ''
    };
  }
  // updateEvents() {
  //   this.eventsModel = [{
  //     title: 'Updaten Event',
  //     start: this.yearMonth + '-08',
  //     end: this.yearMonth + '-10'
  //   }];
  // }
  // get yearMonth(): string {
  //   const dateObj = new Date();
  //   return dateObj.getUTCFullYear() + '-' + (dateObj.getUTCMonth() + 1);
  // }
  dayRender(ev){
    ev.el.addEventListener('dblclick', () => {
       alert('double click!');
    });
  }


total=[];
 div_id:number;
 div_status:number;
   
  eventPublish(e ,i) {
/*
    this.publishdetails.push({ amount:e,
      div_id:this.publishArray[i].id,
      status:this.publishArray[i].status})

      console.log('publishdetails Test ',this.publishdetails)
*/
      //console.log("i value" + i);
      
      // this.div_id=divid;
      // this.div_status= divstatus;

      //console.log(this.div_name)
      /*
      this.total.push(e);
      this.sum = this.total.reduce((x, y) => x + y);
      */
      // this.total.pop();
      // this.sum = this.total.reduce((x, y) => x - y);

     // console.log(this.sum);
     
    //  if(this.isActive){
    //  }
    //  else{
    //   this.total.pop();
    //   this.sum = this.total.reduce((x, y) => x - y);
    //  console.log(this.sum);

    //  }
 

    // console.log(this.total);
    // for(let i=0;i<this.total.length;i++){
    //   this.sum += this.total[i];
      
    // }
  }

  eventpublishbtn(i,event) { 
    var sumPublish=this.teams[i].all_team.length * 1.5;
    if(((document.getElementById("showprompt"+i) as HTMLInputElement).checked)) {
      this.sum =this.sum +sumPublish; 
      this.divArr.push({"div_id":this.teams[i].id,"status":1});
    } else { 
      var SelectID;  
      SelectID=this.teams[i].id; 
      var indexArr=-1;
      this.divArr.filter((mitem,index) => {
        if(mitem.div_id==SelectID) {
          indexArr=index;
        } 
      });
        if(indexArr !=-1) {
          this.divArr.splice(indexArr,1);
        }
        this.sum =this.sum-sumPublish;    
    }
    if(this.divArr.length !=0 ){
      this.enableDivPublish=false;
    }
    if(this.divArr.length ==0 ){
      this.enableDivPublish=true;
    }
    console.log("chetan==",this.divArr);  
  } 
  
  eventUnpublishbtn(i) {
    var sumPublish=this.teams[i].all_team.length * 1.5;
    if(((document.getElementById("checkpublish"+i) as HTMLInputElement).checked)) { 
      this.sumUnpublish =this.sumUnpublish +sumPublish; 
      this.divPublishArr.push({"div_id":this.teams[i].id,"status":0});
    } else { 
      var SelectID;    
      SelectID=this.teams[i].id; 
      var indexArr=-1;
      this.divPublishArr.filter((mitem,index) => {
        if(mitem.div_id==SelectID) {
          indexArr=index;
        } 
      });
        if(indexArr !=-1) {
          this.divPublishArr.splice(indexArr,1);
        }
        this.sumUnpublish =this.sumUnpublish-sumPublish;   
    }
    if(this.divPublishArr.length !=0 ){
      this.enableDivUnPublish=false;
    }
    if(this.divPublishArr.length ==0 ){
      this.enableDivUnPublish=true;
    }
  }

  EventDivPublish() { 
    if(this.sum >this.user.amount) { 
      this._snackBar.open('You do not have enough credits to publish the selected groups. Please purchase credits to continue.', 'Insufficient Credits', {  duration: 10000  });  
    }
    else {      
      if(navigator.onLine) { 
        this.security.DivisionMultiplePublish(this.divArr,this.sum).subscribe(data =>{
          if(data["status"]=="200") { 
            // this.sum=0;
            // this.user.amount=0;
            this.availblecredit=this.availblecredit-this.sum
              this.sum=0
            this.GetDivision();  
            this._snackBar.open(data["mesg"], 'Publish', {  duration: 5000  });      
            return;      
          } else {
            this._snackBar.open('Internal Server Error', 'Error', {  duration: 5000  });   
          } 
        },err => { console.error(err); })
      }
      else {   
        this._snackBar.open('No internet connection.', 'You are Offline', {  duration: 5000  });  
      } 
    } 
  }
 
  EventDivUnPublish() {  
    console.log("this.divPublishArr==",this.divPublishArr);
    console.log("this.divPublishArr==",this.sumUnpublish); 
    // if(navigator.onLine) {  
    //   this.security.DivisionMultiplePublish(this.divPublishArr,this.sumUnpublish).subscribe(data =>{
    //     if(data["status"]=="200") { 
    //       this.GetDivision();  
    //       this._snackBar.open(data["mesg"], 'UnPublish', {  duration: 5000  });      
    //       return;      
    //     } else {
    //       this._snackBar.open('Internal Server Error', 'Error', {  duration: 5000  });   
    //     } 
    //   },err => { console.error(err); })
    // }
    // else {   
    //   this._snackBar.open('No internet connection.', 'You are Offline', {  duration: 5000  });  
    // } 
  }

 



 
 
 
}
