import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UserserviceServiceService} from  '../../userservice-service.service'
import { SecurityServiceService } from 'src/app/security-service.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  
@Input("childMessage") childMessage
 
 
  total:number = 0;
  unitptice:number=0
  delit:number = 0;
  quantity:number;
  title:string;
  
  cartItem=[]
  tempProduct=[]
  payementdetails=[]
  constructor(private Products:SecurityServiceService,
    private route: ActivatedRoute,
    private location: Location,
    public userservice:UserserviceServiceService, 
    private _snackBar: MatSnackBar
    ) { }
    
    
  package =[];

   getItems(): void {
     this.Products.getPrice().subscribe((data )=> {
      this.package = data['package'];
      console.log('cardid',this.package[this.childMessage].id)

  this.cartItem.push({
    id:this.package[this.childMessage].id,
    title:this.package[this.childMessage].title,
    price:this.package[this.childMessage].price,
    content:this.package[this.childMessage].content,
    quantity:this.package[this.childMessage].quantity
    
    })
    this.totalPrice();
    this.UnitPrice();
     });
    }
  ngOnInit() {
console.log('childmessage',this.childMessage)
    //this.totalPrice();
    this.getItems();
  
  this.loadStripe();
  
  //this.UnitPrice();
  console.log(this.cartItem)

 
  }

  ngOnChanges()
  {
    this.cartItem.push({
      id:this.package[this.childMessage].id,
      title:this.package[this.childMessage].title,
      price:this.package[this.childMessage].price,
      content:this.package[this.childMessage].content,
      quantity:this.package[this.childMessage].quantity
      
      })
      this.totalPrice()
      this.UnitPrice();

}
  addToCart() {
    // console.log("addToCart click item is", this.item);
  //  this.cartItem(this.cartItem)
  //console.log(this.cartItem)
  
 }


  

  // getpopup(det) {
  //   this.selectedProduct.next(det);
  // }

 

  delpopup(i){
    console.log(i);
         this.cartItem.splice(i,1);
         this.totalPrice();  
    }

  totalPrice(){
    this.total = 0;
    for(var i=0;i<this.cartItem.length;i++)
    {
      this.total += this.cartItem[i].price *  this.cartItem[i].quantity;
     // alert(this.total)
    }
  }


  UnitPrice(){
    this.unitptice = 0;
    for(var i=0;i<this.cartItem.length;i++)
    {
      this.unitptice += this.cartItem[i].price *  this.cartItem[i].quantity;
     // alert(this.total)
    }
  }
  
  add(pid : number){
   
    for(var i=0;i<this.cartItem.length;i++){
      if(this.cartItem[i].id === pid)
       {  
        this.cartItem[i].quantity = 1+parseInt(this.cartItem[i].quantity)
        }           
      }
      this.totalPrice();
      this.UnitPrice();
  }

  del(pid : number){

     for(var i=0;i<this.cartItem.length;i++){
        if(this.cartItem[i].id === pid)
         {  
           this.cartItem[i].quantity -= 1;
         }           
     }
     
      this.totalPrice();
      this.UnitPrice();
  }



  

  loadStripe() {     
    if(!window.document.getElementById('stripe-script')) {
      var s = window.document.createElement("script");
      s.id = "stripe-script";
      s.type = "text/javascript";
      s.src = "https://checkout.stripe.com/checkout.js";
      window.document.body.appendChild(s);
    }
  }

  openCheckout(amount,i) {

    console.log("quantity" +i)
     

    this.payementdetails.push({amount:amount,
      package_id:this.cartItem[i].id,
    quantity:this.cartItem[i].quantity})
    
    console.log('paymentdetails',this.payementdetails)
    

    if(this.userservice.isLogged()==true){
      var handler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_aeUUjYYcx4XNfKVW60pmHTtI',
        locale: 'auto',
        token: function (token: any) {
          // You can access the token ID with `token.id`.
          // Get the token ID to your server-side code for use.
          //console.log(token)
          //alert('Token Created!!');
          let data = {'stripeToken': token.id, 'amount': amount}
          console.log(data);
          console.log(this.cartItem)
          localStorage.setItem('data', JSON.stringify(data));
          console.log("This is amount and Token id",JSON.parse(localStorage.getItem('data')));
          
        }
      });
    
      handler.open({
        name: 'Payment Gateway',
        description: 'Bracket Team',
        amount: amount * 100
      });
      this.buycredit()
  
    }
    else{
  
        this._snackBar.open("Before Payment", "Please Login", {
          duration: 2000,
        });
    }
    }
    buycredit()
    {
      this.Products.buycredit(this.payementdetails).subscribe((data )=> {
      })
    }



}



