import { Component, OnInit, ViewChild, ElementRef, ɵConsole } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction'
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import { Calendar } from '@fullcalendar/core';
import { SecurityServiceService } from '../security-service.service'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { InfoWindowManager } from '@agm/core';
import { SetschdeuleComponent } from '../setschdeule/setschdeule.component';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
@Component({
    selector: 'app-bookschedule',
    templateUrl: './bookschedule.component.html',
    styleUrls: ['./bookschedule.component.css']
})
export class BookscheduleComponent implements OnInit {

    details = [{ court: "Court 1", venue: "Venue 1", date: "01-01-20", stime: "10:00", etime: "12:00", matchup: "MatchUp 1" },
    { court: "Court 2", venue: "Venue 2", date: "01-01-20", stime: "10:00", etime: "12:00", matchup: "MatchUp 2" },
    { court: "Court 3", venue: "Venue 3", date: "01-01-20", stime: "10:00", etime: "12:00", matchup: "MatchUp 3" }];
    items = [{ court: "Court 1" }, { court: "Court 2" }, { court: "Court 3" }]
    options: any;
    finalarrayvenue = [];
    @ViewChild('fullcalendar') fullcalendar: FullCalendarComponent;
    @ViewChild('external') external: ElementRef;
    public minDate: Date = new Date("05/07/2017 2:00 AM");
    public maxDate: Date = new Date("05/27/2017 11:00 AM");
    public dateValue: Date = new Date("05/16/2017 5:00 AM");
    venue=[] 
    venuematrix = []
    eventsModel: EventInput[] = [
      
    ];
  
    eventsdate = []
    typeteam = '10Teams'
     finalDateArrForMatch = [];
     bracketdata=[]
     roundsdata=[]
     dateindex=0
     date
     dateInChunks: EventInput[] = [
    
    ];
    evenetdatechunks:EventInput=[
      
    ]
    defaultdate
    changeid
    changetext

    dublicatedateICnChunks=[]
    dubclicateevenetdatechunks=[]
    flag
    dublicatefinalDateArrForMatch
    constructor(public security: SecurityServiceService, public dialog: MatDialog) {
        this.flag=3
        this.changetext='test'
        // this.venue.push({
        //     name: 'abc',
        //     court: [{ court_name: 'abc' }, { court_name: 'def' }],
        //     date: [
        //         { first_game_start: '05:00', list_game_start: '06:00', venue_date: '2019-12-27' },
        //         { first_game_start: '06:00', list_game_start: '07:00', venue_date: '2019-12-27' },
        //         { first_game_start: '01:00', list_game_start: '02:00', venue_date: '2019-12-28' },
        //         { first_game_start: '03:00', list_game_start: '04:00', venue_date: '2019-12-29' },
        //         { first_game_start: '08:00', list_game_start: '09:00', venue_date: '2019-12-30' }
        //     ],
        //     duration: '01:00',
        //     matchup: ['m1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9'],
           

        // })
         
       
      
    //    if(this.venue[0].matchup.length==1)
    //    {
    //     this.bracketdata.push(
    //         {matchup:'m1',round:'Round1'}
    //          )
    //    }
    //    else if(this.venue[0].matchup.length==3)
    //    {
    //     this.bracketdata.push(
    //         {matchup:'m1',round:'Round1'},
    //         {matchup:'m2',round:'Round1'},
    //         {matchup:'m3',round:'Round2'}
    //     )
    //    }
    //    else if(this.venue[0].matchup.length==4)
    //    {
    //     this.bracketdata.push(
    //         {matchup:'m1',round:'Round1'},
    //        {matchup:'m2',round:'Round2'},
    //        {matchup:'m3',round:'Round2'},
    //        {matchup:'m4',round:'Round3'}
    //     )
    //    }
    //    else if(this.venue[0].matchup.length==5)
    //    {
    //     this.bracketdata.push(
    //         {matchup:'m1',round:'Round1'},
    //        {matchup:'m2',round:'Round2'},
    //        {matchup:'m3',round:'Round2'},
    //        {matchup:'m4',round:'Round3'}
    //     )
    //    }
    //    else if(this.venue[0].matchup.length==6)
    //    {
    //     this.bracketdata.push(
    //         {matchup:'m1',round:'Round1'},
    //        {matchup:'m2',round:'Round1'},
    //        {matchup:'m3',round:'Round2'},
    //        {matchup:'m4',round:'Round2'},
    //        {matchup:'m5',round:'Round3'}
    //     )
    //    }
    //    else if(this.venue[0].matchup.length==7)
    //    {
    //     this.bracketdata.push(
    //        {matchup:'m1',round:'Round1'},
    //        {matchup:'m2',round:'Round1'},
    //        {matchup:'m3',round:'Round1'},
    //        {matchup:'m4',round:'Round2'},
    //        {matchup:'m5',round:'Round2'},
    //        {matchup:'m6',round:'Round3'}
    //     )
    //    }
    //    else if(this.venue[0].matchup.length==7)
    //    {
    //     this.bracketdata.push(
    //        {matchup:'m1',round:'Round1'},
    //        {matchup:'m2',round:'Round1'},
    //        {matchup:'m3',round:'Round1'},
    //        {matchup:'m4',round:'Round2'},
    //        {matchup:'m5',round:'Round2'},
    //        {matchup:'m6',round:'Round3'}
    //     )
    //    }
    //    else if(this.venue[0].matchup.length==8)
    //    {
    //     this.bracketdata.push(
    //         {matchup:'m1',round:'Round1'},
    //         {matchup:'m2',round:'Round2'},
    //         {matchup:'m3',round:'Round2'},
    //         {matchup:'m4',round:'Round2'},
    //         {matchup:'m5',round:'Round2'},
    //         {matchup:'m6',round:'Round3'},
    //         {matchup:'m7',round:'Round3'},
    //         {matchup:'m8',round:'Round4'}
    //      )
    //    }
      

    //    else if(this.venue[0].matchup.length==9)
    //    {
    //     this.bracketdata.push(
    //        {matchup:'m1',round:'Round1'},
    //        {matchup:'m2',round:'Round1'},
    //        {matchup:'m3',round:'Round2'},
    //        {matchup:'m4',round:'Round2'},
    //        {matchup:'m5',round:'Round2'},
    //        {matchup:'m6',round:'Round2'},
    //        {matchup:'m7',round:'Round3'},
    //        {matchup:'m8',round:'Round3'},
    //        {matchup:'m9',round:'Round4'}
    //         )
    //     }
    //     else if(this.venue[0].matchup.length==10)
    //     {
    //         this.bracketdata.push(
    //             {matchup:'m1',round:'Round1'},
    //             {matchup:'m2',round:'Round1'},
    //             {matchup:'m3',round:'Round1'},
    //             {matchup:'m4',round:'Round2'},
    //             {matchup:'m5',round:'Round2'},
    //             {matchup:'m6',round:'Round2'},
    //             {matchup:'m7',round:'Round2'},
    //             {matchup:'m8',round:'Round3'},
    //             {matchup:'m9',round:'Round3'},
    //             {matchup:'m10',round:'Round4'}
    //              )
    //     }
    //     else if(this.venue[0].matchup.length==11)
    //     {
    //         this.bracketdata.push(
    //             {matchup:'m1',round:'Round1'},
    //             {matchup:'m2',round:'Round1'},
    //             {matchup:'m3',round:'Round1'},
    //             {matchup:'m4',round:'Round1'},
    //             {matchup:'m5',round:'Round2'},
    //             {matchup:'m6',round:'Round2'},
    //             {matchup:'m7',round:'Round2'},
    //             {matchup:'m8',round:'Round2'},
    //             {matchup:'m9',round:'Round3'},
    //             {matchup:'m10',round:'Round3'},
    //             {matchup:'m11',round:'Round4'}
    //              )
    //     }
    //     else if(this.venue[0].matchup.length==12)
    //     {
    //         this.bracketdata.push(
    //             {matchup:'m1',round:'Round1'},
    //             {matchup:'m2',round:'Round1'},
    //             {matchup:'m3',round:'Round1'},
    //             {matchup:'m4',round:'Round1'},
    //             {matchup:'m5',round:'Round1'},
    //             {matchup:'m6',round:'Round2'},
    //             {matchup:'m7',round:'Round2'},
    //             {matchup:'m8',round:'Round2'},
    //             {matchup:'m9',round:'Round2'},
    //             {matchup:'m10',round:'Round3'},
    //             {matchup:'m11',round:'Round3'},
    //             {matchup:'m12',round:'Round4'}
    //              )
    //     }
    //     else if(this.venue[0].matchup.length==15)
    //     {
    //         this.bracketdata.push(
    //             {matchup:'m1',round:'Round1'},
    //             {matchup:'m2',round:'Round1'},
    //             {matchup:'m3',round:'Round1'},
    //             {matchup:'m4',round:'Round1'},
    //             {matchup:'m5',round:'Round1'},
    //             {matchup:'m6',round:'Round1'},
    //             {matchup:'m7',round:'Round1'},
    //             {matchup:'m8',round:'Round1'},
    //             {matchup:'m9',round:'Round2'},
    //             {matchup:'m10',round:'Round2'},
    //             {matchup:'m11',round:'Round2'},
    //             {matchup:'m12',round:'Round2'},
    //             {matchup:'m13',round:'Round3'},
    //             {matchup:'m14',round:'Round3'},
    //             {matchup:'m15',round:'Round4'}
    //              )
    //     }
        
         
    //     console.log('bracketdata',this.bracketdata)
    
       
    //     const courtLen = this.venue[0].court.length; 
    //     const matchLen = this.venue[0].matchup.length; 
    //     let courtModulus = 0;
        
    //     for (var i = 0; i < matchLen; i++) {
            
    //         if(courtModulus == courtLen) {
    //             courtModulus = 0;
    //         }
    //         this.venuematrix.push({
    //             match: this.venue[0].matchup[i],
    //             ground: this.venue[0].court[courtModulus].court_name ,
    //             round:'',
    //             date:''
    //         });
    //         courtModulus++;
    //     }
    //        console.log('venue',this.venuematrix)
    //     const dateLen = this.venue[0].date.length; 
        
    //     let counterForVenueMatrix = 0;
    //     const venueMatrixLen = this.venuematrix.length;

    //     for(let i=0;i<dateLen;i++) {

    //         let tempObj = [];
    //         for(let j=0; j< courtLen; j++) {
    //             if(counterForVenueMatrix < venueMatrixLen) {
    //                 tempObj[j] = this.venuematrix[counterForVenueMatrix]; 
    //                 counterForVenueMatrix++; 
    //             }
    //         }

    //         if( Object.keys(tempObj).length > 0 ) {
    //             this.finalDateArrForMatch.push({
    //                 details: tempObj,
    //                 date: this.venue[0].date[i].venue_date,
    //                 time:[{
    //                 'first_game':this.venue[0].date[i].first_game_start,
    //                 'last_game':this.venue[0].date[i].list_game_start}],
                   

    //             });

    //         }   
            
            
    //     }


    //           console.log('finaldatearray',this.finalDateArrForMatch)
         
    //     for(let i=0;i<this.finalDateArrForMatch.length;i++)
    //     {
          
            
    //         for(let j=0;j<this.finalDateArrForMatch[i].details.length;j++)
    //         {  

        
           
    //           for(let x=0;x<this.bracketdata.length;x++)
    //           {
    //                if(this.finalDateArrForMatch[i].details[j].match==this.bracketdata[x].matchup)
    //                {
    //                 console.log(this.finalDateArrForMatch[i].details[j].match+'--'+this.bracketdata[x].round)
    //                 this.finalDateArrForMatch[i].details[j].round=this.bracketdata[x].round
    //                 this.finalDateArrForMatch[i].details[j].date=this.finalDateArrForMatch[i].date
    //                }
    //           }  
            
    //         } 

            

    //     }
    //     console.log('roundsdata',this.bracketdata)
    //     var d = new Date("2019-12-15"); 


    //     var id=1
    //     this.loaddate(id)
    //     this.scheduleapi() 
    }
    scheduleapi()
    {
    //     this.security.automateschedule().subscribe(data =>
    //    {

    //     })
    }
    loaddatedata()
    {
    //    if(localStorage['evenetdatechunks'])
    //    {
    //        alert('hi')
    //     this.evenetdatechunks=JSON.parse(localStorage['evenetdatechunks'])
    //     console.log('evenetdatechunks',this.evenetdatechunks)
    //     this.defaultdate=this.evenetdatechunks[0].start
    //    }

    //     else{
       
        this.dateInChunks=[]
        this.evenetdatechunks=[]
                 
       console.log('finalDateArrForMatch',this.finalDateArrForMatch)
         
        

         

        for(var i=0;i<this.finalDateArrForMatch.length;i++)
        {
        if(this.date==this.finalDateArrForMatch[i].date)
        {

            this.dateInChunks.push({
        details:this.finalDateArrForMatch[i].details,
        date:this.finalDateArrForMatch[i].date,   
         time:this.finalDateArrForMatch[i].time,
         title: 'Event Now'+i,
        start: this.finalDateArrForMatch[i].date+'T'+this.finalDateArrForMatch[i].time[0].first_game,
         end: this.finalDateArrForMatch[i].date+'T'+this.finalDateArrForMatch[i].time[0].last_game  
            })
        }
        this.dublicatedateICnChunks.push({
           details:this.finalDateArrForMatch[i].details,
           date:this.finalDateArrForMatch[i].date,
           time:this.finalDateArrForMatch[i].time,
           title:'Event Now'+i 
        })



       }
     
      
       for(var i=0;i<this.dublicatedateICnChunks.length;i++)
       {
        for(var j=0;j<this.dublicatedateICnChunks[i].details.length;j++)
        {
            this.dubclicateevenetdatechunks.push({
        title:'Court:'+this.dublicatedateICnChunks[i].details[j].ground,
        matchUp:this.dublicatedateICnChunks[i].details[j].match,
        start:this.dublicatedateICnChunks[i].details[j].date+'T'+this.dublicatedateICnChunks[i].time[0].first_game,
        end:this.dublicatedateICnChunks[i].details[j].date+'T'+this.dublicatedateICnChunks[i].time[0].last_game,  
        round:this.dublicatedateICnChunks[i].details[j].round     
    })
        }
       }
       console.log('dublicateinchunks',this.dubclicateevenetdatechunks)
       for(var i=0;i<this.dateInChunks.length;i++)
       {
        for(var j=0;j<this.dateInChunks[i].details.length;j++)
        {
        this.evenetdatechunks.push({title:'Court:'+this.dateInChunks[i].details[j].ground,
        matchUp:this.dateInChunks[i].details[j].match,
        start:this.dateInChunks[i].details[j].date+'T'+this.dateInChunks[i].time[0].first_game,
        end:this.dateInChunks[i].details[j].date+'T'+this.dateInChunks[i].time[0].last_game,
        round:this.dateInChunks[i].details[j].round           
        
        
        })
        }

       } 
       console.log('eventdatechunks',this.evenetdatechunks) 

       this.defaultdate=this.evenetdatechunks[0].start
       
       console.log('dateInChunks',this.defaultdate)

    // }
    }

    loaddate(id?)
    {
      
        if(this.date!=this.finalDateArrForMatch[this.dateindex].date)
        {
           
         this.date=this.finalDateArrForMatch[this.dateindex].date
         if(id==undefined)
         {
            const calendar=this.fullcalendar.getApi();
            calendar.next()
         }
         
        }
        else if(id==1)
        {
            
            this.date=this.finalDateArrForMatch[this.dateindex].date
            
            const calendar=this.fullcalendar.getApi();
            calendar.next()
           
        }
        else
        {
            
            this.dateindex++
            this.date=this.finalDateArrForMatch[this.dateindex].date
            const calendar=this.fullcalendar.getApi();
            calendar.next()
            
        }
        this.loaddatedata() 
        this.dateindex++
    }
    // showCalendar():void {
    //   $('#datetimepicker1').datetimepicker();
    //  };


    ngOnInit() {
        this.eventload()
        this.venueload()
    }
    venueload()
    {
        this.security.automateschedule().subscribe(data =>
            {
                console.log('venue',data["venue"])
                
              
                
             })
         this.venue.push({
            name: 'abc',
            court: [{ court_name: 'abc' }, { court_name: 'def' }],
            date: [
                { first_game_start: '05:00', list_game_start: '06:00', venue_date: '2019-12-27' },
                { first_game_start: '06:00', list_game_start: '07:00', venue_date: '2019-12-27' },
                { first_game_start: '01:00', list_game_start: '02:00', venue_date: '2019-12-28' },
                { first_game_start: '03:00', list_game_start: '04:00', venue_date: '2019-12-29' },
                { first_game_start: '08:00', list_game_start: '09:00', venue_date: '2019-12-30' }
            ],
            duration: '01:00',
            matchup: ['m1', 'm2', 'm3', 'm4', 'm5', 'm6', 'm7', 'm8', 'm9'],
           

        })
        this.algogenerate()
    }
    algogenerate()
    {
         if(this.venue[0].matchup.length==1)
       {
        this.bracketdata.push(
            {matchup:'m1',round:'Round1'}
             )
       }
       else if(this.venue[0].matchup.length==3)
       {
        this.bracketdata.push(
            {matchup:'m1',round:'Round1'},
            {matchup:'m2',round:'Round1'},
            {matchup:'m3',round:'Round2'}
        )
       }
       else if(this.venue[0].matchup.length==4)
       {
        this.bracketdata.push(
            {matchup:'m1',round:'Round1'},
           {matchup:'m2',round:'Round2'},
           {matchup:'m3',round:'Round2'},
           {matchup:'m4',round:'Round3'}
        )
       }
       else if(this.venue[0].matchup.length==5)
       {
        this.bracketdata.push(
            {matchup:'m1',round:'Round1'},
           {matchup:'m2',round:'Round2'},
           {matchup:'m3',round:'Round2'},
           {matchup:'m4',round:'Round3'}
        )
       }
       else if(this.venue[0].matchup.length==6)
       {
        this.bracketdata.push(
            {matchup:'m1',round:'Round1'},
           {matchup:'m2',round:'Round1'},
           {matchup:'m3',round:'Round2'},
           {matchup:'m4',round:'Round2'},
           {matchup:'m5',round:'Round3'}
        )
       }
       else if(this.venue[0].matchup.length==7)
       {
        this.bracketdata.push(
           {matchup:'m1',round:'Round1'},
           {matchup:'m2',round:'Round1'},
           {matchup:'m3',round:'Round1'},
           {matchup:'m4',round:'Round2'},
           {matchup:'m5',round:'Round2'},
           {matchup:'m6',round:'Round3'}
        )
       }
       else if(this.venue[0].matchup.length==7)
       {
        this.bracketdata.push(
           {matchup:'m1',round:'Round1'},
           {matchup:'m2',round:'Round1'},
           {matchup:'m3',round:'Round1'},
           {matchup:'m4',round:'Round2'},
           {matchup:'m5',round:'Round2'},
           {matchup:'m6',round:'Round3'}
        )
       }
       else if(this.venue[0].matchup.length==8)
       {
        this.bracketdata.push(
            {matchup:'m1',round:'Round1'},
            {matchup:'m2',round:'Round2'},
            {matchup:'m3',round:'Round2'},
            {matchup:'m4',round:'Round2'},
            {matchup:'m5',round:'Round2'},
            {matchup:'m6',round:'Round3'},
            {matchup:'m7',round:'Round3'},
            {matchup:'m8',round:'Round4'}
         )
       }
      

       else if(this.venue[0].matchup.length==9)
       {
        this.bracketdata.push(
           {matchup:'m1',round:'Round1'},
           {matchup:'m2',round:'Round1'},
           {matchup:'m3',round:'Round2'},
           {matchup:'m4',round:'Round2'},
           {matchup:'m5',round:'Round2'},
           {matchup:'m6',round:'Round2'},
           {matchup:'m7',round:'Round3'},
           {matchup:'m8',round:'Round3'},
           {matchup:'m9',round:'Round4'}
            )
        }
        else if(this.venue[0].matchup.length==10)
        {
            this.bracketdata.push(
                {matchup:'m1',round:'Round1'},
                {matchup:'m2',round:'Round1'},
                {matchup:'m3',round:'Round1'},
                {matchup:'m4',round:'Round2'},
                {matchup:'m5',round:'Round2'},
                {matchup:'m6',round:'Round2'},
                {matchup:'m7',round:'Round2'},
                {matchup:'m8',round:'Round3'},
                {matchup:'m9',round:'Round3'},
                {matchup:'m10',round:'Round4'}
                 )
        }
        else if(this.venue[0].matchup.length==11)
        {
            this.bracketdata.push(
                {matchup:'m1',round:'Round1'},
                {matchup:'m2',round:'Round1'},
                {matchup:'m3',round:'Round1'},
                {matchup:'m4',round:'Round1'},
                {matchup:'m5',round:'Round2'},
                {matchup:'m6',round:'Round2'},
                {matchup:'m7',round:'Round2'},
                {matchup:'m8',round:'Round2'},
                {matchup:'m9',round:'Round3'},
                {matchup:'m10',round:'Round3'},
                {matchup:'m11',round:'Round4'}
                 )
        }
        else if(this.venue[0].matchup.length==12)
        {
            this.bracketdata.push(
                {matchup:'m1',round:'Round1'},
                {matchup:'m2',round:'Round1'},
                {matchup:'m3',round:'Round1'},
                {matchup:'m4',round:'Round1'},
                {matchup:'m5',round:'Round1'},
                {matchup:'m6',round:'Round2'},
                {matchup:'m7',round:'Round2'},
                {matchup:'m8',round:'Round2'},
                {matchup:'m9',round:'Round2'},
                {matchup:'m10',round:'Round3'},
                {matchup:'m11',round:'Round3'},
                {matchup:'m12',round:'Round4'}
                 )
        }
        else if(this.venue[0].matchup.length==15)
        {
            this.bracketdata.push(
                {matchup:'m1',round:'Round1'},
                {matchup:'m2',round:'Round1'},
                {matchup:'m3',round:'Round1'},
                {matchup:'m4',round:'Round1'},
                {matchup:'m5',round:'Round1'},
                {matchup:'m6',round:'Round1'},
                {matchup:'m7',round:'Round1'},
                {matchup:'m8',round:'Round1'},
                {matchup:'m9',round:'Round2'},
                {matchup:'m10',round:'Round2'},
                {matchup:'m11',round:'Round2'},
                {matchup:'m12',round:'Round2'},
                {matchup:'m13',round:'Round3'},
                {matchup:'m14',round:'Round3'},
                {matchup:'m15',round:'Round4'}
                 )
        }
        
         
        console.log('bracketdata',this.bracketdata)
    
       
        const courtLen = this.venue[0].court.length; 
        const matchLen = this.venue[0].matchup.length; 
        let courtModulus = 0;
        
        for (var i = 0; i < matchLen; i++) {
            
            if(courtModulus == courtLen) {
                courtModulus = 0;
            }
            this.venuematrix.push({
                match: this.venue[0].matchup[i],
                ground: this.venue[0].court[courtModulus].court_name ,
                round:'',
                date:''
            });
            courtModulus++;
        }
           console.log('venue',this.venuematrix)
        const dateLen = this.venue[0].date.length; 
        
        let counterForVenueMatrix = 0;
        const venueMatrixLen = this.venuematrix.length;

        for(let i=0;i<dateLen;i++) {

            let tempObj = [];
            for(let j=0; j< courtLen; j++) {
                if(counterForVenueMatrix < venueMatrixLen) {
                    tempObj[j] = this.venuematrix[counterForVenueMatrix]; 
                    counterForVenueMatrix++; 
                }
            }

            if( Object.keys(tempObj).length > 0 ) {
                this.finalDateArrForMatch.push({
                    details: tempObj,
                    date: this.venue[0].date[i].venue_date,
                    time:[{
                    'first_game':this.venue[0].date[i].first_game_start,
                    'last_game':this.venue[0].date[i].list_game_start}],
                   

                });

            }   
            
            
        }


              console.log('finaldatearray',this.finalDateArrForMatch)
         
        for(let i=0;i<this.finalDateArrForMatch.length;i++)
        {
          
            
            for(let j=0;j<this.finalDateArrForMatch[i].details.length;j++)
            {  

        
           
              for(let x=0;x<this.bracketdata.length;x++)
              {
                   if(this.finalDateArrForMatch[i].details[j].match==this.bracketdata[x].matchup)
                   {
                    console.log(this.finalDateArrForMatch[i].details[j].match+'--'+this.bracketdata[x].round)
                    this.finalDateArrForMatch[i].details[j].round=this.bracketdata[x].round
                    this.finalDateArrForMatch[i].details[j].date=this.finalDateArrForMatch[i].date
                   }
              }  
            
            } 

            

        }
        console.log('roundsdata',this.bracketdata)
        var d = new Date("2019-12-15"); 


        var id=1
        this.loaddate(id)
        this.scheduleapi() 
    }
    eventload() {
        // var todayDate = moment().startOf('day');

        // var TODAY = todayDate.format('YYYY-MM-DD');
        // console.log('today', TODAY)
        // this.finalarrayvenue = JSON.parse(localStorage.getItem('FinalArrayVenue'))
        // console.log('finalarrayvenue', JSON.parse(localStorage.getItem('FinalArrayVenue')))
        // Don't use FullcalendarOption interface
        
        this.options = {
            editable: true,
           
            defaultDate: '2019-12-27',
            customButtons: {
                myCustomButton: {
                    text: 'custom!',
                    click: function () {
                        alert('clicked the custom button!');
                    }
                }
            },
            theme: 'bootstrap4', // default view, may be bootstrap
            header: {
                left: 'prev,next today myCustomButton',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
                //  right: 'dayGridMonth,timeGridWeek,timeGridDay'
                //right: 'month,agendaWeek,agendaDay,listMonth'
            },
            // weekNumbers: true,
            eventLimit: true, // allow "more" link when too many events
            // events: [
            //     {
            //         title: 'Meeting',
            //         start: TODAY + 'T10:30:00',
            //         end: TODAY + 'T12:30:00'
            //     }],
            schedulerLicenseKey: '0217308380-fcs-1576048266',
     
            resources:[
                { id: 'a', title: 'Resource A' },
                { id: 'b', title: 'Resource B' }
              ],
    //           resources: [
    //     { id: 'a', title: 'Resource A' },
    //     { id: 'b', title: 'Resource B' }
    //   ],
    //   events:  [
    //     { title: 'Event Now', start: new Date(),resourceId: 'a' }
    // ],
            //   eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {
            //     alert(
            //         event.title + " was moved " +
            //         dayDelta + " days and " +
            //         minuteDelta + " minutes."
            //     );
            //     if (allDay) {
            //         alert("Event is now all-day");
            //     }else{
            //         alert("Event has a time-of-day");
            //     }
            //     if (!confirm("Are you sure about this change?")) {
            //         revertFunc();
            //     }
            // },
            // eventClick: function(info) {
            //   alert('Event: ' + info.event.title);
            //   alert('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
            //   alert('View: ' + info.view.type);

            //   // change the border color just for fun
            //   info.el.style.borderColor = 'red';
            // },

            plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin, resourceTimelinePlugin]
        };

        new Draggable(this.external.nativeElement, {
            itemSelector: '.fc-event',
            eventData: function (eventEl) {
                console.log(eventEl)
                return {
                    title: eventEl.innerText
                };
            }
        });

        // this.security.getbookvenue().subscribe(data=>{
        //   console.log('data',data)

        //   // this.router.navigate(['create-tournament']);
        // })



    }

    dayRender(ev) {
        ev.el.addEventListener('dblclick', () => {
            alert('double click!');
        });
    }
    eventDrop (event,dayDelta,minuteDelta,allDay,revertFunc) {
            alert(
                event.title + " was moved " +
                dayDelta + " days and " +
                minuteDelta + " minutes."
            );
            if (allDay) {
                alert("Event is now all-day");
            }else{
                alert("Event has a time-of-day");
            }
            if (!confirm("Are you sure about this change?")) {
                revertFunc();
            }
        }
    eventClick(info,i) {
        console.log('idd',i)
        console.log(info.event)
        console.log('information',info.event.extendedProps.matchUp)
        console.log('dateInChunks',this.evenetdatechunks)
              
        for(var j=0;j<this.evenetdatechunks.length;j++)
        {
            console.log('infos',info.event.extendedProps.matchUp+'=='+this.evenetdatechunks[j].matchUp)
               if(info.event.extendedProps.matchUp==this.evenetdatechunks[j].matchUp)
               {
                       this.changeid=j
               }
        }
        console.log('changeid',this.changeid)
        // change the border color just for fun
        console.log(this.evenetdatechunks.length)
        info.el.style.borderColor = 'red';
      
       
        this.dublicatefinalDateArrForMatch=this.finalDateArrForMatch
        localStorage['finalDateArrForMatch']= JSON.stringify(this.dublicatefinalDateArrForMatch)
        const dialogRef = this.dialog.open(SetschdeuleComponent, {
            width: '550px',
            data: {
                 name: info.event.title,
               court:this.venue[0].court,
               venue:this.evenetdatechunks,
               date:info.event.start,
            //    finalDateArrForMatch:this.dublicatefinalDateArrForMatch,
               matchupid:this.evenetdatechunks[this.changeid].matchUp,
               flag:this.flag
            }

        });

        dialogRef.afterClosed().subscribe(result => {
            if(result.flag==1)
            {
                 alert('hi')
                 console.log('finalDateArrForMatch',this.finalDateArrForMatch)
            }
            else if(result.flag==3)
            {
                alert('dataInChunks')
          //   window.location.reload();
            console.log('results',result.date)
          this.evenetdatechunks[this.changeid].start=new Date(result.date)
          //  this.evenetdatechunks[this.changeid].start='2019-12-18T06:00'
          this.evenetdatechunks[this.changeid].end=new Date(result.date)
          //   this.evenetdatechunks[this.changeid].end='2019-12-18T06:00'
            this.evenetdatechunks[this.changeid].title=result.court
            console.log('dateInChunks',this.evenetdatechunks)
         
            
            this.loaddatedata1()
            }
        });

        
    }

    loaddatedata1()
    {
    
        const calendar=this.fullcalendar.getApi();
        calendar.removeAllEventSources()
        calendar.addEventSource(this.evenetdatechunks)  
        

    }



}
