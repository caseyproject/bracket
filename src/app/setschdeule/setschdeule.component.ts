import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormControl, FormBuilder, Validators,FormGroup } from '@angular/forms';
export interface DialogData {
  name: string;
  court:string;
  venue:string;
  date:string;
  finalDateArrForMatch:string;
  matchupid:string;
  flag:string;
}
@Component({
  selector: 'app-setschdeule',
  templateUrl: './setschdeule.component.html',
  styleUrls: ['./setschdeule.component.css']
})

export class SetschdeuleComponent implements OnInit {
  items=['Court1','Court2','Court3']
  gettitle
  court
  venue
  date
  dateValue
  court1
  valuecourt
  selected
  startSchedule:FormGroup;
  finalDateArrForMatch
  finalDateArrForMatch1
  matchupid
  getid
  flag
  constructor(private formBuilder: FormBuilder,public dialogRef: MatDialogRef<SetschdeuleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
     
      this.gettitle=this.data.name;
      this.court=this.data.court
      this.venue=this.data.venue 
      this.date=this.data.date
      this.dateValue=this.date
      this.matchupid=this.data.matchupid
      // this.finalDateArrForMatch=this.data.finalDateArrForMatch
      // this.finalDateArrForMatch1=this.finalDateArrForMatch
      this.flag=this.data.flag
     console.log('finalDateArrForMatch',JSON.parse(localStorage['finalDateArrForMatch']))
      this.finalDateArrForMatch1=JSON.parse(localStorage['finalDateArrForMatch'])
this.startSchedule = this.formBuilder.group({
  courtname: ['', Validators.required],
  dateValue:['', Validators.required]
})
     }

  ngOnInit() {
  }
  save()
  {
    this.closeModalDialog(true);
  }
  setSortOrder(ev)
 {
   console.log(ev,'hiii')
 }
  closeModalDialog(ModalStatus) {  
  
   
  console.log(this.matchupid)
   
    
    console.log('startSchedule',this.startSchedule.value.courtname); // Returns false  
    
    
         
     
  
    
   
    for(var i=0;i<this.finalDateArrForMatch1.length;i++)
    {
      this.finalDateArrForMatch1[i].date=this.finalDateArrForMatch1[i].date+'T'+this.finalDateArrForMatch1[i].time[0].first_game
    

    }

    for(var i=0;i<this.finalDateArrForMatch1.length;i++)
    {
      console.log('length',this.finalDateArrForMatch1[i].details.length)
     for(var j=0;j<this.finalDateArrForMatch1[i].details.length;j++)
     {
       console.log(this.finalDateArrForMatch1[i].details[j].match+'=='+this.matchupid)
          if(this.finalDateArrForMatch1[i].details[j].match==this.matchupid)
          {
           this.finalDateArrForMatch1[i].date=this.startSchedule.value.dateValue   
           
          }
          
     } 
         
    }
    console.log('dubclicateevenetdatechunks',this.finalDateArrForMatch1)
    for(var b=0;b<this.finalDateArrForMatch1.length;b++)
    {
        if(this.finalDateArrForMatch1[b].date==this.startSchedule.value.dateValue)
        {
          this.getid=b
        }
    }

    // for(var x=0;x<this.finalDateArrForMatch.length;x++)
    // {
    
    //   if(x!=0 && x!=this.finalDateArrForMatch.length-1 )
    //   {

        console.log('xxx',this.getid)
        console.log('date1',new Date(this.finalDateArrForMatch1[this.getid].date))
        // console.log('date2',new Date(this.finalDateArrForMatch[this.getid-1].date)) 
        // console.log('date3',new Date(this.finalDateArrForMatch[this.getid+1].date)) 

        if(this.getid-1!=-1 && this.getid+1!=this.finalDateArrForMatch1.length)
        {
         if(new Date(this.finalDateArrForMatch1[this.getid].date)>new Date(this.finalDateArrForMatch1[this.getid-1].date) &&new Date(this.finalDateArrForMatch1[this.getid].date)< new Date(this.finalDateArrForMatch1[this.getid+1].date))
         {
           alert('a')
           this.flag=3
               this.dialogRef.close({
      gettitle:this.gettitle, 
      court: this.startSchedule.value.courtname,
      venue:this.venue,
      date:this.startSchedule.value.dateValue,
      ModalStatus:ModalStatus,
      flag:this.flag
    });
             
         }
         else 
         {
          this.flag=1
          this.dialogRef.close({flag:this.flag });
         }
        }
        else if(this.getid-1==-1)
        {
          
          if(new Date(this.finalDateArrForMatch1[this.getid].date)< new Date(this.finalDateArrForMatch1[this.getid+1].date))
          {
            alert('b')
            this.flag=3
                this.dialogRef.close({
       gettitle:this.gettitle, 
       court: this.startSchedule.value.courtname,
       venue:this.venue,
       date:this.startSchedule.value.dateValue,
       ModalStatus:ModalStatus,
       flag:this.flag 
      });
              
          }
          else
          {
            this.flag=1
          this.dialogRef.close({flag:this.flag });
          }
        }
        else if(this.getid+1==this.finalDateArrForMatch1.length)
        {
          if(new Date(this.finalDateArrForMatch1[this.getid].date)>new Date(this.finalDateArrForMatch1[this.getid-1].date) )
          {
            alert('c')
            this.flag=3
                this.dialogRef.close({
       gettitle:this.gettitle, 
       court: this.startSchedule.value.courtname,
       venue:this.venue,
       date:this.startSchedule.value.dateValue,
       ModalStatus:ModalStatus,
       flag:this.flag
      });
              
          } 
          else
          {
            this.flag=1
          this.dialogRef.close({flag:this.flag });
          }   
        }

    //     }
    // }
    // this.dialogRef.close({
    //   gettitle:this.gettitle, 
    //   court: this.startSchedule.value.courtname,
    //   venue:this.venue,
    //   date:this.startSchedule.value.dateValue,
    //   ModalStatus:ModalStatus }); // does not close the dialog
    }

}
