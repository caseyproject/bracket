import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetschdeuleComponent } from './setschdeule.component';

describe('SetschdeuleComponent', () => {
  let component: SetschdeuleComponent;
  let fixture: ComponentFixture<SetschdeuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetschdeuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetschdeuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
