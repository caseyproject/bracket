import { Component, OnInit } from '@angular/core';

import{SecurityServiceService}from'../security-service.service'
import { from } from 'rxjs';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {

  constructor(private Products:SecurityServiceService) { }
  package =[];
  title="We offer two pricing options";
  getItems(): void {
    this.Products.getPrice().subscribe((data )=> {
      
      this.package = data['package'];
      
    });
    
  }
  ngOnInit() {
    this.getItems();


  }
}
  
